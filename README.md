Dans client.py affiche une interface graphique avec les champs textes pour récuperer les valeurs ip, api_key et hostname et un bouton
récupération des champs

webserv envoie une requete à Shodan et extrait les valeurs de longitude et latitude depuis l'objet json retouné par Shodan et les envoie, à son tour, en tant que réponse

client.py recoit la réponse et extrait les longitude et latitude pour en former une requete sous la forme:
https://www.openstreetmap.org/?mlat=<lat>&mlon=<long>#map=12
où <lat> et <long> seront remplacés par les valeurs correcpondantes.

Cette requete sera éxécutée en ouvrant dans le navigateur une géolocation des coordonées envoyées.

